package rubin.firebasedemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Firebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Button buttonSunny = (Button) findViewById(R.id.buttonSunny);
        Button buttonFoggy = (Button) findViewById(R.id.buttonFoggy);
        final TextView mTextCondiion = (TextView) findViewById(R.id.textViewCondition);

        firebase = new Firebase("https://incandescent-inferno-7663.firebaseio.com/condition");

        firebase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String newCondition = (String) dataSnapshot.getValue();
                mTextCondiion.setText(newCondition);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        buttonSunny.setOnClickListener(this);
        buttonFoggy.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSunny:
                firebase.setValue("Sunny");
                break;
            case R.id.buttonFoggy:
                firebase.setValue("Foggy");
                break;
        }
    }
}
